# tinymce-v5-package

Contains tinymce 5 beta as an npm package.

## Install

```bash
$ npm install bitbucket:ayanbora/tinymce-v5-package
```
_or_

```bash
$ npm install -s https://bitbucket.org/ayanbora/tinymce-v5-package
```