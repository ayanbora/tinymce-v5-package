(function () {
var legacyoutput = (function () {
    'use strict';

    var global = tinymce.util.Tools.resolve('tinymce.PluginManager');

    global.add('legacyoutput', function (editor) {
      console.error('Legacy output plugin has not yet been updated for TinyMCE 5');
      return;
    });
    function Plugin () {
    }

    return Plugin;

}());
})();
