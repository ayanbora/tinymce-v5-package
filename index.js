export const tinymce = require('./tinymce.min.js')
export const theme = require('./themes/silver/theme.min.js')

export default {
	tinymce: tinymce,
	theme: theme
}